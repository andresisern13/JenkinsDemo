package com.udemy.vaadin.FirstVaadin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FirstVaadinApplication {

	public static void main(String[] args) {
		SpringApplication.run(FirstVaadinApplication.class, args);
	}
}
