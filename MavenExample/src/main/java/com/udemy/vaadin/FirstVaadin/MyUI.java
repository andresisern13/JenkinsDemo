package com.udemy.vaadin.FirstVaadin;


import org.junit.internal.TextListener;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;


@SpringUI(path="/ui")
@Title("This is the title")
@Theme("valo")
public class MyUI extends UI {

	@Override
	protected void init(VaadinRequest request) {
		
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		
		
		Label label1 = new Label("This is a text label content.", ContentMode.TEXT);
		Label label2 = new Label("<b><i>This is a html label content.</b></i>", ContentMode.HTML);
		
		TextField text = new TextField("Name: ");
		text.setValue("Andres");
		text.setMaxLength(10);
		
		Label label3 = new Label();
		label3.setValue(text.getValue());
		
		horizontalLayout.addComponent(label1);
		horizontalLayout.addComponent(label2);
		
		horizontalLayout.addComponent(text);
		
		setContent(horizontalLayout);
	}

}
